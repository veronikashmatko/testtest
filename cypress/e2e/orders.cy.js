describe("Страница раздела 'Оформить заказ'", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Test 1: При нажатии на логотип открывается главная страница КП  ", () => {
    cy.get(".el-button.el-button--primary").eq(1).click(); //нажать на кнопку "оформить заказ" в блоке версии
    cy.get(".kp-header__logo").eq(0).click(); //нажать на логотип для открытия главной страницы
    cy.get(".col-12").should("exist").should("be.visible"); //отображение блоков главной страницы
  });
});
